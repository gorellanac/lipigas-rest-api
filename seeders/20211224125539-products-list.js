'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Products', [
      {
        name: 'Chocolate',
        price: 3000,
        quantity: 100,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Milk',
        price: 900,
        quantity: 1000,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Cookies',
        price: 100,
        quantity: 100,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]);
  },

  down: async (queryInterface, Sequelize) => {
     return queryInterface.bulkDelete('Products', null, {});
  }
};
