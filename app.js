const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');

const oauthRouter = require('./routes/oauth');
const apiRouter = require('./routes/api');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

// Routes
app.use('/', oauthRouter);
app.use('/api', apiRouter);

// Handler errors
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    success:false,
    message: err.message
  });
});

module.exports = app;
