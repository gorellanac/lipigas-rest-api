const bcrypt = require('bcrypt');
const ClientModel = require('../models').Client;
const TokenModel = require('../models').Token;
const UserModel = require('../models').User;
const ModelAlreadyExists = require('../exceptions/ModelAlreadyExists');

module.exports.getAccessToken = async function(bearerToken) {
    return await TokenModel.findOne(
        {
            where: {
                accessToken: bearerToken
            },
            include: [
                {
                    model: ClientModel,
                    as: 'client'
                },
                {
                    model: UserModel,
                    as: 'user'
                }
            ]
        }
    ).then((token) => {
        const data = new Object();
        for(const prop in token.get()) {
            data[prop] = token[prop];
        }
        data.client = data.client.get();
        data.user = data.user.get();

        return data;
    }).catch((error) => console.error(error));
};

module.exports.getClient = async function(clientId, clientSecret) {
    return await ClientModel.findOne(
        {
            where: {
                clientId: clientId,
                clientSecret: clientSecret
            },
            raw: true
        }
    ).catch((error) => console.error(error));
};

module.exports.getRefreshToken = async function(refreshToken) {
    return await TokenModel.findOne(
        {
            where: {
                refreshToken: refreshToken
            },
            include: [
                {
                    model: ClientModel,
                    as: 'client'
                },
                {
                    model: UserModel,
                    as: 'user'
                }
            ]
        }
    ).then((token) => {
        const data = new Object();
        for(const prop in token.get()) {
            data[prop] = token[prop];
        }
        data.client = data.client.get();
        data.user = data.user.get();

        return data;
    }).catch((error) => console.error(error));
};

module.exports.getUser = async function(username, password) {
    return await UserModel.findOne(
        {
            where: {
                username: username
            }
        }
    ).then((user) => {
        const isMatch = bcrypt.compareSync(password, user.get().password);
        if(isMatch) {
            return user.get();
        } else {
            console.error('Password does not match');
        }
    }).catch((error) => console.error(error));
};

module.exports.saveToken = async function(token, client, user) {
    return await TokenModel.create(
        {
            userId: user.id,
            clientId: client.id,
            accessToken: token.accessToken,
            accessTokenExpiresAt: token.accessTokenExpiresAt,
            refreshToken: token.refreshToken,
            refreshTokenExpiresAt: token.refreshTokenExpiresAt
        }
    ).then((token) => {
        const data = new Object();
        for(const prop in token.get()) {
            data[prop] = token[prop];
        }

        data.client = data.clientId;
        data.user = data.userId;

        return data;
    }).catch((error) => console.error(error));
};

module.exports.revokeToken = async function(token) {
    return await TokenModel.findOne(
        {
            where: {
                refreshToken: token.refreshToken
            }
        }
    ).then((refreshToken) => {
        return refreshToken
            .destroy()
            .then(() => {
                return !!refreshToken;
            }).catch((error) => console.error(error))
    }).catch((error) => console.error(error));
};

/**
 * Set a new client to grant access to resources
 * 
 * @param {*} client req.body from request
 * 
 * @returns ClientModel
 */
module.exports.setClient = async function(client) {

    const obj = await ClientModel.findOne({
        where: {
            clientId: client.clientId,
            clientSecret: client.clientSecret
        }
    });

    if(obj) {
        throw new ModelAlreadyExists('clientId and clientSecret already exist in the system');
    }
    
    return await ClientModel.create({
        clientId: client.clientId,
        clientSecret: client.clientSecret,
        redirectUris: client.redirectUris,
        grants: client.grants,
    }).then((client) => {
        client = client && typeof client == 'object' ? client.toJSON() : client;
        const data = new Object();
        for(const prop in client) {
            data[prop] = client[prop];
        }
        data.client = data.clientId;
        data.grants = data.grants;

        return data;
    }).catch((error) => console.error(error));
};

/**
 * Create a new user
 * 
 * @param {*} user req.body from request
 * 
 * @returns UserModel
 */
module.exports.setUser = async function(user) {

    const obj = await UserModel.findOne({
        where: {
            username: user.username
        }
    });
    if(obj !== null) {
        throw new ModelAlreadyExists('username already exists in the system');
    }

    return await UserModel.create({
        username: user.username,
        password: user.password,
        name: user.name,
    }).then((userRes) => {
        userRes = userRes && typeof userRes == 'object' ? userRes.toJSON() : userRes;
        const data = new Object();
        for(const prop in userRes) {
            data[prop] = userRes[prop];
        }
        data.username = data.username;
        data.name = data.name;

        return data;
    }).catch((error) => console.error(error));
};