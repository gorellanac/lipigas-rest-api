const ProductModel = require('../models').Product;
const SaleModel = require('../models').Sale;
const SaleDetailModel = require('../models').SaleDetail;
const UserModel = require('../models').User;
const ModelNotFound = require('../exceptions/ModelNotFound');
const ActionNotAllowedError = require('../exceptions/ActionNotAllowedError');
const EmptyRequestError = require('../exceptions/EmptyRequestError');
const Sequelize = require('sequelize');

/**
 * Get all user sales
 * 
 * @param {*} user  User sales
 * 
 * @returns Sale[]
 */
module.exports.getUserSales = async (user) => {

    const _userId = user.userId;

    // Check if user exists
    if(await UserModel.findByPk(_userId) === null) {
        throw new ModelNotFound('User does not exist in the system');
    }

    return await SaleModel.findAll({
        where: {
            userId: _userId
        },
        include: 'details'
    }).then((sales) => {
        return sales;
    }).catch((error) => console.error(error));
};

/**
 * Get a sale by its id
 * 
 * @param {*} user 
 * 
 * @returns Sale
 */
module.exports.getSale = async (req) => {

    const _userId = req.userId;
    const _saleId = req.saleId;

    const userObj = await UserModel.findByPk(_userId);
    const saleObj = await SaleModel.findByPk(_saleId);
    // Check user exists
    if(userObj === null || saleObj === null) {
        throw new ModelNotFound('Elements do not exist in the system');
    }
    // Check if user is related to sale
    if(userObj.id !== saleObj.userId) {
        throw new ActionNotAllowedError();
    }
    
    return saleObj;
};

/**
 * Create a sale if user id is correct and at least 1 product is set
 * 
 * @param {*} req 
 * @returns 
 */
module.exports.createSale = async (req) => {

    const userId = req.userId;
    const products = req.products;

    if(await UserModel.findByPk(userId) === null) {
        throw new ModelNotFound('User does not exist in the system');
    }
    if(typeof products === 'undefined' || products.length == 0) {
        throw new EmptyRequestError('Any product has been added to the new sale');
    }
    // Check for products
    let sum = 0;
    for(let i=0; i<products.length;i++) {
        const prod = products[i];
        const prodObj = await ProductModel.findByPk(prod.id);
        if(prodObj === null) {
            throw new ModelNotFound(`Product #${prod.id} does not exist in the system`);
        }
        const subtotal = prodObj.price * prod.quantity;
        sum += subtotal;
    }
    const tax = sum * 0.19;
    
    // Create all elements as transaction
    return SaleModel.sequelize.transaction(function (t) {
        // 
        return SaleModel.create({
            userId: userId,
            tax: tax,
            total: sum,
        }, {transaction: t}).then(function (sale) {
            let details = [];
            for(let i=0; i<products.length;i++) {
                const prod = products[i];
                details.push({
                    saleId: sale.id,
                    productId: prod.id,
                    quantity: prod.quantity,
                    subtotal: prod.price * prod.quantity
                });
            }

            return SaleDetailModel.bulkCreate(details, {transaction: t});
        });
      
      }).then(function(result) {
        return result;
      }).catch((error) => console.error(error));
};

/**
 * Update sale
 * 
 * @param {*} req 
 * @returns 
 */
module.exports.updateSale = async (saleId, req) => {

    const userId = req.userId;
    const products = req.products;

    const userObj = await UserModel.findByPk(userId);
    const saleObj = await SaleModel.findByPk(saleId);

    // Check user exists
    if(userObj === null) { throw new ModelNotFound('User does not exist in the system'); }
    if(saleObj === null) { throw new ModelNotFound('Sale does not exist in the system'); }

    // Check if user is related to sale
    if(userObj.id !== saleObj.userId) {
        throw new ActionNotAllowedError();
    }

    // Check for products
    if(typeof products === 'undefined' || products.length == 0) {
        throw new EmptyRequestError('Any product has been added to the new sale');
    }
    let sum = 0;
    const ll = products.length;
    for(let i=0; i<ll;i++) {
        const prod = products[i];
        const detail = await SaleDetailModel.findOne({
            where: {
                saleId: saleId,
                productId: prod.id,
            }
        });

        if(detail === null) {
            throw new ModelNotFound(`Product #${prod.id} for sale ${saleId} does not exist in the system`);
        }

        const subtotal = prod.price * prod.quantity;
        sum += subtotal;
    }
    const tax = sum * 0.19;
    
    // Create all elements as transaction
    return saleObj.sequelize.transaction(async function (t) {

        // Update sale values
        saleObj.set({
            userId: userId,
            tax: tax,
            total: sum,
        });
        await saleObj.save();

        // Update details sale
        const l = products.length;
        for(let i=0; i<l;i++) {
            const prod = products[i];
            const detail = await SaleDetailModel.findOne({
                where: {
                    saleId: saleId,
                    productId: prod.id,
                }
            });

            if(detail !== null) {
                detail.set({
                    quantity: prod.quantity,
                    subtotal: prod.price * prod.quantity
                });
                await detail.save();
            }
        }

        return {
            sale: saleObj
        };
    
      
      }).then(function(result) {
        return result;
      }).catch((error) => console.error(error));
};

/**
 * Delete sale
 * 
 * @param {*} req 
 * @returns 
 */
module.exports.deleteSale = async (saleId, userId) => {

    const userObj = await UserModel.findByPk(userId);
    const saleObj = await SaleModel.findByPk(saleId);

    if(userObj === null) { throw new ModelNotFound('User does not exist in the system'); }
    if(saleObj === null) { throw new ModelNotFound('Sale does not exist in the system'); }

    // Check if user is related to sale
    if(userObj.id !== saleObj.userId) {
        throw new ActionNotAllowedError();
    }
    
    await saleObj.destroy();
    return true;
};
