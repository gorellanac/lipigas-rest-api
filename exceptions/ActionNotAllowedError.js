const ApplicationError = require('./ApplicationError');

class ActionNotAllowedError extends ApplicationError {
  constructor(message) {
    super(message || 'You have no authorization to do this action', 404);
  }
}

module.exports = ActionNotAllowedError;