const ApplicationError = require('./ApplicationError');

class ModelNotFound extends ApplicationError {
  constructor(message) {
    super(message || 'Object does not exist.', 404);
  }
}

module.exports = ModelNotFound;