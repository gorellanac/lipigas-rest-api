const ApplicationError = require('./ApplicationError');

class ModelAlreadyExists extends ApplicationError {
  constructor(message) {
    super(message || 'Model already exists.', 401);
  }
}

module.exports = ModelAlreadyExists;