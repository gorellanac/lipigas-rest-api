const ApplicationError = require('./ApplicationError');

class EmptyRequestError extends ApplicationError {
  constructor(message) {
    super(message || 'Object does not exist.', 400);
  }
}

module.exports = EmptyRequestError;