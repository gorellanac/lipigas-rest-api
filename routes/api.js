var express = require('express');
var router = express.Router();
const SaleController = require('../controllers/salescontroller');
const OAuthController = require('../controllers/oauthcontroller');
const OAuthServer = require('express-oauth-server');
const validations = require('./validations/api');
const { validationResult } = require('express-validator');

/* Apply custom controller on OAuthServer */
router.oauth = new OAuthServer({
  model: OAuthController
});

/**
 * Get all user sales
 * 
 * @method GET
 */
router.get('/sales/:userId', [validations.getSalesValidation, router.oauth.authenticate()], (req, res, next) => {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
  }

  SaleController.getUserSales(req.params)
    .then((sales) =>
      res.json(
        {
          'sales': sales
        })
    ).catch((error) => {
      return next(error);
    });
});

/**
 * Get puntual sale by user and sale id
 * 
 * @method GET
 */
router.get('/sales/:saleId/:userId', [validations.getSaleValidation, router.oauth.authenticate()], (req, res, next) => {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
  }

  SaleController.getSale(req.params)
    .then((sale) =>
      res.json(
        {
          'sale': sale
        })
    ).catch((error) => {
      return next(error);
    });
});

/**
 * Create a new sale with products
 * 
 * @method POST
 */
router.post('/sales', [validations.createSaleValidation, router.oauth.authenticate()], (req, res, next) => {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
  }

  SaleController.createSale(req.body)
    .then((transaction) =>
      res.json(
        {
          'results': transaction
        })
    ).catch((error) => {
      return next(error);
    });
});

/**
 * Update an entire sale with their products
 * 
 * @method PUT
 */
router.put('/sales/:saleId', [validations.updateSaleValidation, router.oauth.authenticate()], (req, res, next) => {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
  }

  SaleController.updateSale(req.params.saleId, req.body)
    .then((transaction) =>
      res.json(
        {
          'results': transaction
        })
    ).catch((error) => {
      return next(error);
    });
});


/**
 * Delete sale
 * 
 * @method DELETE
 */
router.delete('/sales/:saleId/:userId', [validations.deleteSaleValidation, router.oauth.authenticate()], (req, res, next) => {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
  }

  SaleController.deleteSale(req.params.saleId, req.params.userId)
    .then((deleted) =>
      res.json(
        {
          'result': deleted
        })
    ).catch((error) => {
      return next(error);
    });
});


router.get('/testing', router.oauth.authenticate(), function(req, res) {
  res.json('You just have entered to my secret place!');
});

module.exports = router;
