const { check } = require('express-validator');
 
exports.setClientValidation = [
    check('clientId', 'clientId is required').not().isEmpty(),
    check('clientSecret', 'clientSecret is required').not().isEmpty(),
    check('redirectUris', 'redirectUris is required').not().isEmpty(),
    check('grants', 'grants is required').not().isEmpty(),
]

exports.signupValidation = [
    check('username', 'username must be a valid email').isEmail().normalizeEmail({ gmail_remove_dots: true }),
    check('password', 'Password must be 8 or more characters').isLength({ min: 8 })
]