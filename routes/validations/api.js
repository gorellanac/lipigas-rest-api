const { check } = require('express-validator');
 
exports.getSalesValidation = [
    check('userId', 'userId is required').not().isEmpty()
]
exports.getSaleValidation = [
    check('userId', 'userId is required').not().isEmpty(),
    check('saleId', 'saleId is required').not().isEmpty()
]
exports.createSaleValidation = [
    check('userId', 'userId is required').not().isEmpty(),
    check('products', 'products is required and be an array with at least 1 element').not().isEmpty(),
]
exports.updateSaleValidation = [
    check('saleId', 'saleId is required').not().isEmpty(),
    check('userId', 'userId is required').not().isEmpty(),
    check('products', 'products is required and be an array with at least 1 element').not().isEmpty(),
]
exports.deleteSaleValidation = [
    check('userId', 'userId is required').not().isEmpty(),
    check('saleId', 'saleId is required').not().isEmpty()
]