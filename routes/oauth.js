const express = require('express');
const router = express.Router();
const OAuthController = require('../controllers/oauthcontroller');
const OAuthServer = require('express-oauth-server');
const { setClientValidation, signupValidation } = require('./validations/oauth');
const { validationResult } = require('express-validator');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* Apply custom controller on OAuthServer */
router.oauth = new OAuthServer({
  model: OAuthController
});

/* OAuth Routes with fields validation */
router.post('/oauth/token', router.oauth.token());

router.post('/oauth/set_client', setClientValidation, (req, res, next) => {
  
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
  }

  OAuthController.setClient(req.body)
    .then((client) => res.json(client))
    .catch((error) => {
      return next(error);
    });
});

router.post('/oauth/signup', signupValidation, function(req, res, next) {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
  }

  OAuthController.setUser(req.body)
    .then((user) => res.json(user))
    .catch((error) => {
      return next(error);
    });
});

module.exports = router;
